package com.azawisza.shop;

import ch.qos.logback.classic.Logger;
import com.google.common.base.Predicate;
import org.fluentlenium.adapter.FluentTest;
import org.fluentlenium.adapter.util.SharedDriver;
import org.fluentlenium.core.Fluent;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {TestWebMvcConfiguration.class})
@IntegrationTest({
        "server.port:8071",
        "spring.datasource.db-port:9095",
        "chromedriveros:chromedriver_linux",
        "spring.datasource.db-web-port:8083"
})
@WebAppConfiguration
@SharedDriver(type = SharedDriver.SharedType.PER_CLASS)
public abstract class SeleniumTestBase extends FluentTest {

    @Value("${server.port}")
    protected String applicationPort;

    protected static final String confirm = "/confirm";
    protected static final String select = "/select";

    static {
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(ch.qos.logback.classic.Level.WARN);
    }


    @BeforeClass
    public static void setUp() throws Exception {
        String chromedriverOs = System.getProperty("chromedriveros","chromedriver_linux");
        System.out.println(" ---> chrome driver : "+chromedriverOs);
        System.setProperty("webdriver.chrome.driver", "src/test/resources/"+ chromedriverOs);
    }

    @Page
    protected UiTestHelper helper;

    @Override
    public WebDriver getDefaultDriver() {
        ChromeOptions options = new ChromeOptions();
        WebDriver webDriver = new ChromeDriver(options);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        return webDriver;
    }

    protected void loginAndGoToPage(String path) {
        loginAndGoToPage(path, "1");
    }

    protected void loginAndGoToPage(String path, String customerID) {
        goToPage(path);
        Cookie cookie = new Cookie("customerID", customerID);
        getDriver().manage().addCookie(cookie);
        goToPage(path);
        helper.await().atMost(10, TimeUnit.SECONDS).untilPage().isLoaded();
    }

    protected void awaitForCatalogContent() {
        helper.await().atMost(10, TimeUnit.SECONDS).until(new Predicate<Fluent>() {
            @Override
            public boolean apply(Fluent fluent) {
                List<String> ids = fluent.find("input[name=productCheckbox]").getIds();
                return ids != null && !ids.isEmpty();
            }
        });
    }

    protected void awaitForConfirmationPage() {
        helper.await().atMost(10, TimeUnit.SECONDS).until(new Predicate<Fluent>() {
            @Override
            public boolean apply(Fluent fluent) {
                return fluent.url().contains("confirm");
            }
        });
    }

    protected void goToPage(String path) {
        goTo("http://localhost:" + applicationPort + path);
    }

    protected int countBasketItems() {
        int size = helper.find("div[name=productBasket]").size();
        return size;
    }

    protected FluentWebElement getProductInBasketWithName(String id) {
        FluentWebElement first = getWithNameAndId("div[name=productBasket]", id);
        return first;
    }

    protected FluentWebElement getProductWithName(String id) {
        return getWithNameAndId("input[name=productCheckbox]", id);
    }

    protected FluentWebElement getSubmitButton() {
        return getWithNameAndId("input[name=checkout]", "checkout");
    }

    protected FluentWebElement getWithNameAndId(String name, String id) {
        FluentList<FluentWebElement> checkboxes = helper.find(name);
        for (FluentWebElement checkbox : checkboxes) {
            if (id.equals(checkbox.getId())) {
                return checkbox;
            }
        }
        return null;
    }

}
