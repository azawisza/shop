package com.azawisza.shop;

import org.fluentlenium.core.FluentPage;

/**
 * Created by azawisza
 */
public class UiTestHelper extends FluentPage {

    public void openPage() {
        withDefaultUrl("localhost:8070/register/registerForm");
    }
}
