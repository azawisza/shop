package com.azawisza.shop.tests.catalogue;

import com.azawisza.shop.IntegrationTestBase;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;

import javax.servlet.http.Cookie;

import static com.azawisza.shop.TestHelper.basicCatalogue;
import static com.azawisza.shop.TestHelper.krakowResponse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by azawisza
 */
public class CatalogueServiceApiTest extends IntegrationTestBase {

    @Test
    public void shouldRetrieveCatalogOnLocation() throws Exception {
        //given
        String krakowLocation = "KRAKOW";
        //when
        ResultActions result = mockMvc.perform(get("/catalogue/" + krakowLocation)
                .cookie(new Cookie("customerID", "1")));
        //then
        result.andExpect(content().json(krakowResponse))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnBasicCatalog_forNotExistingLocation() throws Exception {
        //given
        String location = "anyNotExisting";
        //when
        ResultActions result = mockMvc.perform(get("/catalogue/" + location)
                .cookie(new Cookie("customerID", "1")));
        //then
        result.andExpect(content().json(basicCatalogue))
                .andExpect(status().isOk());
    }

    @Test(expected = Exception.class)
    public void shouldNotReturnBasicCatalog_forLocationNotSpecified() throws Exception {
        //given
        //when
        ResultActions resultNoSlash = mockMvc.perform(get("/catalogue")
                .cookie(new Cookie("customerID", "1")));
        //then
        resultNoSlash.andExpect(content().json(basicCatalogue))
                .andExpect(status().isOk());
    }


}
