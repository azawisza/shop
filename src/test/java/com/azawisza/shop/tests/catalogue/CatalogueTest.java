package com.azawisza.shop.tests.catalogue;

import com.azawisza.shop.TestHelper;
import com.azawisza.shop.services.catalogue.api.Product;
import com.azawisza.shop.services.catalogue.api.ProductsCategory;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import org.junit.Test;

import java.util.List;

import static com.azawisza.shop.TestHelper.getCategories;
import static com.azawisza.shop.TestHelper.product;
import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza
 */
public class CatalogueTest {

    @Test
    public void shouldReturnProduct() {
        //given
        Product product = product("LOC", "1", "PROD", "CATEGORY");
        Catalogue catalogue = TestHelper.catalogue(product);
        //when
        List<ProductsCategory> categories = getCategories(catalogue);
        ProductsCategory productsCategory = categories.get(0);
        Product productFromCatalogue = productsCategory.getProducts().iterator().next();
        //then
        assertThat(productFromCatalogue).isEqualTo(product);
        assertThat(productsCategory.getName()).isEqualTo("CATEGORY");
    }

}