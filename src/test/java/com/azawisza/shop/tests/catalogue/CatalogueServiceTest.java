package com.azawisza.shop.tests.catalogue;

import com.azawisza.shop.IntegrationTestBase;
import com.azawisza.shop.services.catalogue.CatalogueService;
import com.azawisza.shop.services.catalogue.api.ProductsCategory;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.webui.confirm.calculate.ProductIdCalculator;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.azawisza.shop.TestHelper.*;
import static com.google.common.collect.ImmutableList.of;
import static com.google.common.collect.Sets.newHashSet;
import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza
 */
public class CatalogueServiceTest extends IntegrationTestBase {

    @Autowired
    private CatalogueService service;

    @Autowired
    private ProductIdCalculator calculator;

    @Test
    public void shouldFindProductsUsingId() {
        //given
        Catalogue products = service.collectCatalogue(newHashSet(2L, 4L));
        //when
        List<ProductsCategory> categories = getCategories(products);
        //then
        assertThat(categories).isEqualTo(
                of(new ProductsCategory().withName("Smartphones").withProducts(of(
                        product(null, "4", "Samsung", "Smartphones")
                        )
                ), new ProductsCategory().withName("Computers").withProducts(of(
                        product("WARSAW", "2", "Desktop", "Computers")
                        )
                )));
    }

    @Test
    public void shouldFindProductsOfLocation() {
        //given
        Catalogue products = service.getCatalogueForLocation("WARSAW");
        //when
        List<ProductsCategory> categories = getCategories(products);
        //then
        assertThat(categories).isEqualTo(of(warsawCategory, basicCategory));
    }

    @Test
    public void shouldNotReturnEmptyCatalogue() {
        //given
        Catalogue products = service.getCatalogueForLocation("NOT_FOUND");
        //when
        boolean empty = getCategories(products).isEmpty();
        //then
        Assertions.assertThat(empty).isFalse();
    }

    @Test
    public void shouldReturnEmptyCatalogueForId() {
        //given
        Catalogue products = service.collectCatalogue(newHashSet(123L));
        //when
        boolean empty = getCategories(products).isEmpty();
        //then
        Assertions.assertThat(empty).isTrue();
    }

    @Test
    public void shouldHandleNullAndRepetition() {
        //given
        ArrayList<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add(null);
        Set<Long> products = calculator.toDistinctLong(list);
        //when
        int onlyTwo = products.size();
        //then
        Assertions.assertThat(onlyTwo).isEqualTo(2);
    }


}

