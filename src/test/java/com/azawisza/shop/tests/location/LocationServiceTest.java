package com.azawisza.shop.tests.location;

import com.azawisza.shop.IntegrationTestBase;
import com.azawisza.shop.services.location.CustomerLocationService;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Created by azawisza
 */
public class LocationServiceTest extends IntegrationTestBase {

    @Autowired
    private CustomerLocationService service;

    @Test
    public void shouldNotReturnLocation() {
        //given
        //when
        Optional<String> customerLocation = service.findCustomerLocation("123");
        //then
        Assertions.assertThat(customerLocation.isPresent()).isFalse();
    }
}
