package com.azawisza.shop.tests.location;

import com.azawisza.shop.IntegrationTestBase;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;

import javax.servlet.http.Cookie;

import static com.azawisza.shop.TestHelper.location;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by azawisza
 */
public class LocationServiceApiTest extends IntegrationTestBase {

    @Test
    public void shouldRetrieveCatalogOnLocation() throws Exception {
        //given
        //when
        ResultActions result = mockMvc.perform(get("/customerlocation/")
                .cookie(new Cookie("customerID", "1")));
        ResultActions resultNoSlassh = mockMvc.perform(get("/customerlocation")
                .cookie(new Cookie("customerID", "1")));
        //then
        result.andExpect(content().json(location))
                .andExpect(status().isOk());
        resultNoSlassh.andExpect(content().json(location))
                .andExpect(status().isOk());
    }
}
