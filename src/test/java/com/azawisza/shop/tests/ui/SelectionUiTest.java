package com.azawisza.shop.tests.ui;

import com.azawisza.shop.SeleniumTestBase;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza
 */
public class SelectionUiTest extends SeleniumTestBase {



    @Test
    public void shouldDisplayCatalogue() {
        //given
        loginAndGoToPage(select);
        awaitForCatalogContent();
        FluentWebElement element = getProductWithName("Samsung");
        FluentWebElement element2 = getProductWithName("IPhone");
        FluentWebElement krakow = getProductWithName("Computer 100");
        //when
        boolean checkoutEnabled = getSubmitButton().isEnabled();
        int inBasket = countBasketItems();
        //then
        assertThat(inBasket).isEqualTo(0);
        assertThat(checkoutEnabled).isFalse();
        assertThat(element).isNotNull();
        assertThat(element2).isNotNull();
        assertThat(krakow).isNotNull();

    }

    @Test
    public void shouldAddToBasket() {
        //given
        loginAndGoToPage(select);
        awaitForCatalogContent();
        FluentWebElement prod = getProductWithName("Samsung");
        prod.click();
        //when
        int inBasket = countBasketItems();
        FluentWebElement productInBasketWithName = getProductInBasketWithName("Samsung");
        String idOfBasketEleemnt = productInBasketWithName.getId();
        boolean checkoutEnabled = getSubmitButton().isEnabled();
        //then
        assertThat(inBasket).isEqualTo(1);
        assertThat(idOfBasketEleemnt).isEqualTo("Samsung");
        assertThat(checkoutEnabled).isTrue();
    }

    @Test
    public void shouldRemoveFromBasket() {
        //given
        loginAndGoToPage(select);
        awaitForCatalogContent();
        FluentWebElement prod = getProductWithName("Samsung");
        prod.click();
        int inBasketBefore = countBasketItems();
        //when
        prod.click();
        int inBasket = countBasketItems();
        boolean checkoutEnabled = getSubmitButton().isEnabled();
        //then
        assertThat(inBasketBefore).isEqualTo(1);
        assertThat(inBasket).isEqualTo(0);
        assertThat(checkoutEnabled).isFalse();
    }
}
