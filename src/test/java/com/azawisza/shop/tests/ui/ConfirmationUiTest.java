package com.azawisza.shop.tests.ui;

import com.azawisza.shop.SeleniumTestBase;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza
 */
public class ConfirmationUiTest extends SeleniumTestBase {



    @Test
    public void shouldSelectOrderAndCheckout() {
        //given
        loginAndGoToPage(select);
        awaitForCatalogContent();
        getProductWithName("Samsung").click();
        getProductWithName("IPhone").click();
        int inBasket = countBasketItems();
        boolean checkoutEnabled = getSubmitButton().isEnabled();
        //when
        getSubmitButton().click();
        awaitForConfirmationPage();
        //then
        assertThat(helper.find("li[name='Samsung']").size()).isEqualTo(1);
        assertThat(helper.find("li[name='IPhone']").size()).isEqualTo(1);
    }

}
