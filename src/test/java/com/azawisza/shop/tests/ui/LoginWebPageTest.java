package com.azawisza.shop.tests.ui;

import com.azawisza.shop.SeleniumTestBase;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza
 */
public class LoginWebPageTest extends SeleniumTestBase {



    @Test
    public void shouldShowInfoWhenNotLoggedInOnSelectionPage() throws InterruptedException {
        //given
        //when
        goToPage(select);
        //then
        assertThat(helper.find("#notLogged").getText()).isEqualTo("You are not logged in.");
    }

    @Test
    public void shouldLoginToProductSelection() throws InterruptedException {
        //given
        loginAndGoToPage(select);
        //when
        //then
        assertThat(helper.find("#customerIdHeader").getText()).isEqualTo("Customer ID: 1");
    }

    @Test
    public void shouldExceptionWhenNotLoggedInOnConfirmationPage() {
        //given
        //when
        goToPage(confirm);
        helper.await().atMost(10, TimeUnit.SECONDS).untilPage().isLoaded();
        //then
        assertThat(helper.find("#errorCodeTitle").getText()).contains("Error Code:");
    }

    @Test
    public void shouldDisplayMessage_WhenUserNotInTheSystem() {
        //given
        String customerIdOfUserNotInTheSystem = "3";
        loginAndGoToPage(select, customerIdOfUserNotInTheSystem);
        goToPage(confirm);
        //when
        helper.await().atMost(10, TimeUnit.SECONDS).untilPage().isLoaded();
        //then
        assertThat(helper.find("#NOT_FOUND").size()).isEqualTo(1);
    }

    @Test
    public void shouldDisplayMessage_WhenUserOfInvalidId() {
        //given
        String customerIdOfUserNotInTheSystem = "3a";
        loginAndGoToPage(confirm, customerIdOfUserNotInTheSystem);
        //when
        helper.await().atMost(10, TimeUnit.SECONDS).untilPage().isLoaded();
        //then
        assertThat(helper.find("#INVALID_USER_ID").size()).isEqualTo(1);
    }

    @Test///
    public void shouldLoginAndDisplayEmptyConfirmationPage() {
        //given
        loginAndGoToPage(confirm);
        //when
        //then
        assertThat(helper.find("#customerIdHeader").getText()).isEqualTo("Customer ID: 1");
    }


}
