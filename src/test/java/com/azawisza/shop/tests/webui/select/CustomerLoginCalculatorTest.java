package com.azawisza.shop.tests.webui.select;

import com.azawisza.shop.IntegrationTestBase;
import com.azawisza.shop.webui.select.CustomerLoginCalculator;
import com.azawisza.shop.webui.select.CustomerStatus;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.azawisza.shop.webui.select.CustomerStatus.*;

/**
 * Created by azawisza
 */
public class CustomerLoginCalculatorTest extends IntegrationTestBase {

    @Autowired
    private CustomerLoginCalculator calculator;

    @Test
    public void shouldCalculateWhenNull() {
        //given
        //when
        CustomerStatus calculate = calculator.calculateCustomerId(null);
        //then
        Assertions.assertThat(calculate).isEqualTo(NOT_LOGGED_IN);
    }

    @Test
    public void shouldCalculateWhenNumber() {
        //given
        //when
        CustomerStatus calculate = calculator.calculateCustomerId("123");
        //then
        Assertions.assertThat(calculate).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldCalculateWhenNotNumber() {
        //given
        //when
        CustomerStatus calculate = calculator.calculateCustomerId("asd");
        //then
        Assertions.assertThat(calculate).isEqualTo(INVALID_USER_ID);
    }
}