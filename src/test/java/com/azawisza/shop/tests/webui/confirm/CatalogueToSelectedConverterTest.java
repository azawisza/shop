package com.azawisza.shop.tests.webui.confirm;

import com.azawisza.shop.TestHelper;
import com.azawisza.shop.services.catalogue.api.Product;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.webui.confirm.SelectedProduct;
import com.azawisza.shop.webui.confirm.calculate.CatalogueToSelectedConverter;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.List;

import static com.azawisza.shop.TestHelper.product;
import static com.google.common.collect.ImmutableList.of;

/**
 * Created by azawisza
 */
public class CatalogueToSelectedConverterTest {

    @Test
    public void shouldConvert() {
        //given
        Product product = product("LOC", "1", "PROD", "CATEGORY");
        Catalogue catalogue = TestHelper.catalogue(product);
        CatalogueToSelectedConverter converter = new CatalogueToSelectedConverter();
        //when
        List<SelectedProduct> selectedProducts = converter.convertToProducts(catalogue);
        //then
        Assertions.assertThat(selectedProducts).isEqualTo(of(new SelectedProduct()
                .withCategory("CATEGORY").withName("PROD")));
    }

    @Test
    public void shouldHandleEmpty() {
        //given
        CatalogueToSelectedConverter converter = new CatalogueToSelectedConverter();
        //when
        List<SelectedProduct> selectedProducts = converter.convertToProducts(TestHelper.emptyCatalogue());
        //then
        Assertions.assertThat(selectedProducts).isEmpty();
    }

}