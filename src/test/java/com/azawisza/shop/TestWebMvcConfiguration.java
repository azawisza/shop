package com.azawisza.shop;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.azawisza.shop.configuration.queue.JMSSQSConfig;
import com.azawisza.shop.configuration.queue.SQSConfig;
import com.azawisza.shop.services.audit.EventService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * Created by user on 4/17/17.
 */
public class TestWebMvcConfiguration extends WebMvcConfiguration{

    @Bean
    @Primary
    public DefaultMessageListenerContainer jmsListenerContainer(){
        return Mockito.mock(DefaultMessageListenerContainer.class);
    }

    @Bean
    @Primary
    public EventService mockEventService(){
        return Mockito.mock(EventService.class);
    }

    @Bean
    @Primary
    public JmsTemplate createJMSTemplate(){
        return Mockito.mock(JmsTemplate.class);
    }

    @Bean
    @Primary
    public AmazonSQSClient createSQSClient(){
        return Mockito.mock(AmazonSQSClient.class);
    }

    @Bean
    @Primary
    public LettuceConnectionFactory createLettuceConnectionFactory(){
        return Mockito.mock(LettuceConnectionFactory.class);
    }


}
