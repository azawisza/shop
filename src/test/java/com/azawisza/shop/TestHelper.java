package com.azawisza.shop;

import com.azawisza.shop.services.catalogue.api.CatalogueRS;
import com.azawisza.shop.services.catalogue.api.Product;
import com.azawisza.shop.services.catalogue.api.ProductsCategory;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.services.location.LocationRS;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.ImmutableList.of;

/**
 * Created by azawisza
 */
public class TestHelper {

    public static Gson gson = new Gson();

    public static final ProductsCategory basicCategory = new ProductsCategory()
            .withName("Smartphones")
            .withProducts(of(
                    product(null, "4", "Samsung", "Smartphones"),
                    product(null, "5", "IPhone", "Smartphones")));

    public static final ProductsCategory krakowCategory = new ProductsCategory()
            .withName("Computers")
            .withProducts(of(
                    product("KRAKOW", "3", "Computer 100", "Computers")
            ));

    public static final ProductsCategory warsawCategory = new ProductsCategory()
            .withName("Computers")
            .withProducts(of(
                    product("WARSAW", "1", "IBM Laptop", "Computers"),
                    product("WARSAW", "2", "Desktop", "Computers")
            ));

    public static final String krakowResponse = gson.toJson(new CatalogueRS()
            .withCategory(basicCategory)
            .withCategory(krakowCategory));

    public static final String location = gson.toJson(new LocationRS().withLocation("KRAKOW"));

    public static final String basicCatalogue = gson.toJson(new CatalogueRS()
            .withCategory(basicCategory));

    public static Product product(String location, String productId, String name, String category) {
        return new Product()
                .withCategory(category)
                .withName(name).withLocation(location).withProductId(productId);
    }

    public static Catalogue catalogue(Product product) {
        Multimap<String, Product> result = ArrayListMultimap.create();
        result.put(product.getCategory(), product);
        return new Catalogue(result);
    }

    public static Catalogue emptyCatalogue() {
        Multimap<String, Product> result = ArrayListMultimap.create();
        return new Catalogue(result);
    }

    public static List<ProductsCategory> getCategories(Catalogue catalogue) {
        List<ProductsCategory> result = new ArrayList<>();
        catalogue.forEach(result::add);
        return result;
    }
}
