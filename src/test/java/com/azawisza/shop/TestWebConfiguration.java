package com.azawisza.shop;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.azawisza.shop.configuration.RedisSession;
import com.azawisza.shop.configuration.queue.JMSSQSConfig;
import com.azawisza.shop.configuration.queue.SQSConfig;
import com.azawisza.shop.services.audit.EventService;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by azawisza
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.azawisza.shop"},excludeFilters = {
        @Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = {
                        SQSConfig.class,
                        JMSSQSConfig.class,
                        RedisSession.class
                })
})
@PropertySource(value={"classpath:application-test.yml"})
@EnableTransactionManagement
public class TestWebConfiguration extends TestWebMvcConfiguration {


}