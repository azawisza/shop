<!-- Created by azawisza -->
<html>
<#import "/spring.ftl" as spring/>
<head>
    <title>Confirmation</title>
</head>

<script src="/resources/lib/jquery/jquery-3.1.1.min.js"></script>
<!-- bootstrap -->

<script src="/resources/lib/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<link rel="stylesheet" href="/resources/lib/bootstrap-3.3.7-dist/css/bootstrap.css"/>
<link rel="stylesheet" href="/resources/lib/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"/>
<script src="/resources/lib/ui-bootstrap-tpls-0.13.3.js"></script>
<body>


<#if customerID??>
    <div id="customerIdHeader">Customer ID: ${customerID}</div>
</#if>

    <div class="well well-lg ">
<#if products??>
    Selected products:
    <ul>
    <#list products as product>
      <li name="${product.name}"> ${product.category} - ${product.name}
    </#list>
    <ul>
</#if>
</div>

<#if status ?? && status == 'INVALID_USER_ID'>
    <div id="INVALID_USER_ID"> The user id is invalid.</div>
</#if>
<#if status ?? && status == 'NOT_FOUND'>
    <div id="NOT_FOUND"> User not found.</div>
</#if>


</body>
</html>