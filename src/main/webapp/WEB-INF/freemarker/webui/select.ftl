<!-- Created by azawisza -->
<!doctype html>
<#import "/spring.ftl" as spring/>
<html lang="en" ng-app="reg" xmlns:ng="http://angularjs.org">
<head>
    <meta charset="utf-8">
    <title>Select products</title>
    <link rel="stylesheet" href="resources/css/app.css"/>
</head>


<body>

<#if customerID??>
    <h3 id="customerIdHeader">Customer ID: ${customerID}</h3>

<script src="resources/lib/jquery/jquery-3.1.1.min.js"></script>

<!-- bootstrap -->
<script src="resources/lib/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<link rel="stylesheet" href="resources/lib/bootstrap-3.3.7-dist/css/bootstrap.css"/>
<link rel="stylesheet" href="resources/lib/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"/>

<!-- angular -->
<script src="resources/lib/angular-1.4.1/angular.js"></script>
<script src="resources/lib/angular-1.4.1/angular-route.js"></script>
<script src="resources/lib/angular-1.4.1/angular-resource.js"></script>
<script src="resources/lib/angular-1.4.1/angular-animate.js"></script>


<script src="resources/lib/ui-bootstrap-tpls-0.13.3.js"></script>




<!-- Habitast APPLICATION-->
<!-- order of those is important-->
<script src="resources/js/app.js"></script>

<!-- Main View-->
<script src="resources/js/mainview/MainViewModule.js"></script>
<script src="resources/js/mainview/MainViewController.js"></script>

<!-- Search-->
<script src="resources/js/select/OrderSelectionModule.js"></script>
<script src="resources/js/select/SelectController.js"></script>
<script src="resources/js/select/SubmitOrder.js"></script>
<script src="resources/js/select/SelectionModel.js"></script>
<script src="resources/js/select/LocationService.js"></script>
<script src="resources/js/select/GetCustomerLocation.js"></script>
<script src="resources/js/select/CatalogueService.js"></script>
<script src="resources/js/select/OrderConfirmationService.js"></script>



<!-- Main view-->
<div><span app-version></span></div>
<div ng-controller="MainViewController">
    <div class="well well-lg ">
    <get-customer-location>
        <div id="searchContainer" style="width:100%;height:400px">
            <ng:include src="'resources/js/select/SelectOrderView.html'"></ng:include>
        </div>
    </get-customer-location>
    </div>
</div>
<#else>
  <div id="notLogged">You are not logged in.</div>
</#if>

<#if status ?? && status == 'INVALID_USER_ID'>
    The user id is invalid.
</#if>
<#if status ?? && status == 'NOT_FOUND'>
    <div id="NOT_FOUND"> User not found.</div>
</#if>

</body>
</html>