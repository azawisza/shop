<!-- Created by azawisza -->
<html>
<head>
    <title>Error </title>
</head>
<body>
<br>
<br>

<h3>Application error</h3>

<#if error??>
<b id="errorCodeTitle">Error Code:</b>${error.code}<br>
<b>Message:</b>   ${error.message}<br>
<b>Details:</b>
<pre style="background-color: #c4e3f3">${error.details}</pre>
</#if>

</body>
</html>