angular.module('reg.search')
    .controller('SearchController', ['$scope', '$rootScope', 'SearchModel', 'HabitatSelectionModel',
        function ($scope, $rootScope, SearchModel, HabitatSelectionModel) {

            $scope.updateBasket = function (elementForBasket) {
                    console.log("Updating basket: "+elementForBasket);
                    var product = $.parseJSON(elementForBasket.target.id);
                    var productIndexInBasket;
                    var found = $.map(SearchModel.basket, function(val,index) {
                        if(val.productId == product.productId ){
                            productIndexInBasket = index;
                            return index;
                        }
                    });
                    if(found.length!==0){
                        SearchModel.basket.splice(productIndexInBasket, 1);
                    }else {
                        SearchModel.basket.push(product);
                    }
                    SearchModel.checkoutDisabled = SearchModel.basket.length<1;
                };


            $scope.$on("categoriesUpdated", function (event, reload) {
                    console.log("Updating model categories count: "+SearchModel.categories.length);
                    SearchModel.foundCount = SearchModel.categories.length;
            });


            $scope.searchModel = SearchModel;

        }])
;
