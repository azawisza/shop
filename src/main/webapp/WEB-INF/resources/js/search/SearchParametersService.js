/**
 * Created by cx2 on 2015-09-01.
 */
var registerModule = angular.module('reg.search');

registerModule.factory('SearchParametersService', ['$resource', '$http',
    function ($resource, $http) {
        return {
            getParameters: function () {
                console.log("about to call service ..");
                return $http.get('/catalogue/KRAKOW');
            }
        };
    }]);
