angular.module('reg.search').factory('SearchModel', [function () {
    var searchModel = {
        basket: [],
        checkoutDisabled: true,
        categories: []
    };
    return searchModel;
}]);