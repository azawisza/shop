/**
 * Created by cx2 on 2015-09-01.
 */
var registerModule = angular.module('reg.search');
registerModule.directive('searchForHabitats', ['SearchService', 'SearchModel', '$filter', '$rootScope','$window',
    function (SearchService, SearchModel, $filter, $rootScope, $window) {

        var onSuccess = function (value, responseHeaders) {
            console.log("success =" + value);
            if(!value.errorCode){
                $window.location.href = '/confirm';
            }
        };

        var onFail = function (httpResponse) {
            console.log("failure call result =" + httpResponse);
        };

        var convertQuery = function (searchModel) {
            var request = {};
            if(!$.isEmptyObject(searchModel.basket)){
                var productIdsToPurchase = [];
                $.each(searchModel.basket, function(index,value) {
                  productIdsToPurchase.push(value.productId);
                });
                request.selectedProducts = productIdsToPurchase;
            }
            return request;
        };

        var isNotEmpty = function (text) {
            return text !== null && text !== "" && text !== undefined && text !== "Wszystkie" && text !== "any";
        };

        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.bind('click', function (e) {
                    var request = convertQuery(SearchModel);
                    SearchService.searchHabitats(request,onSuccess,onFail);
                });
            }
        };
    }]);