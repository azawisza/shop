/**
 * Created by cx2 on 2015-09-01.
 */
var registerModule = angular.module('reg.search');
registerModule.directive('loadSearchFormData', ['$rootScope','SearchParametersService', 'SearchModel',
    function ($rootScope, SearchParametersService, SearchModel) {

        var onSuccess = function (value, responseHeaders) {
            console.log("Successful call result =" + value.data);
            if(value.data !== null){
                SearchModel.categories = value.data.categories;
                $rootScope.$broadcast('categoriesUpdated', 1);
            }else {
                console.log("Empty response");
            }
        };

        var onFail = function (httpResponse) {
            console.log("Failure call result =" + httpResponse);
        };
        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.ready(function (e) {
                    var parameters = SearchParametersService.getParameters();
                    parameters.then(onSuccess,onFail);
                });
            }
        };

    }]);