var registerModule = angular.module('reg.search');

registerModule.factory('SearchService', ['$resource', '$http',
    function ($resource, $http) {
        return {
            searchHabitats: function (request,onSuccess,onFailure) {
                 $http.post('/confirm/', request)
                                                    .success(onSuccess)
                                                    .error(onFailure);
            }
        };
    }]);