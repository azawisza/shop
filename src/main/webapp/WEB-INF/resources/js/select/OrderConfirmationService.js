// Created by azawisza
var registerModule = angular.module('order.select');

registerModule.factory('OrderConfirmationService', ['$resource', '$window',
    function ($resource, $window) {
        return {
            confirmOrder: function (request) {
                 $window.location.href = '/confirm/'+request;
            }
        };
    }]);