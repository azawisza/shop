// Created by azawisza
var registerModule = angular.module('order.select');

registerModule.factory('LocationService', ['$http',
    function ($http) {
        return {
            findCustomerLocation: function () {
                return $http.get('/customerlocation/');
            }
        };
    }]);
