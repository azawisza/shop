// Created by azawisza
angular.module('order.select').factory('SelectionModel', [function () {
    var selectionModel = {
        customerID : "",
        customerLocation: "",
        basket: [],
        checkoutDisabled: true,
        statusMessage: "",
        categories: []
    };
    return selectionModel;
}]);