// Created by azawisza
var registerModule = angular.module('order.select');
registerModule.directive('getCustomerLocation', ['$rootScope','LocationService', 'SelectionModel',
    function ($rootScope, LocationService, SelectionModel) {

        var onSuccess = function (value, responseHeaders) {
            console.log("Successful call result =" + value.data);
            if(value.data !== null){
                var locationId = value.data.locationId;
                if(locationId === null || locationId === undefined) {
                    SelectionModel.statusMessage = "Location information not found";
                } else {
                    SelectionModel.customerLocation = value.data.locationId;
                    $rootScope.$broadcast('customerLocationUpdated', 1);
                }
            }else {
                SelectionModel.statusMessage = "Customer information not found";
            }
        };

        var onFail = function (httpResponse) {
            console.log("Failed call result =" + httpResponse);
            SelectionModel.statusMessage = "There was a problem retrieving customer information";
        };
        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.ready(function (e) {
                    var parameters = LocationService.findCustomerLocation();
                    parameters.then(onSuccess,onFail);
                });
            }
        };

    }]);