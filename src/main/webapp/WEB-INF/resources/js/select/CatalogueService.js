// Created by azawisza
var registerModule = angular.module('order.select');

registerModule.factory('CatalogueService', ['$http','SelectionModel',
    function ($http, SelectionModel) {
           var onSuccess = function (value, responseHeaders) {
                if(value.data !== null){
                    SelectionModel.categories = value.data.categories;
                }else {
                    SelectionModel.statusMessage = "Catalogue of products empty.";
                }
            };

            var onFail = function (httpResponse) {
                console.log("Failure call result =" + httpResponse);
                SelectionModel.statusMessage = "Catalogue of products not available.";
            };

        return {
            loadCatalogue: function (locationId) {
                    var parameters = $http.get('/catalogue/'+locationId);
                     parameters.then(onSuccess,onFail);

            }
        };
    }]);
