// Created by azawisza
var registerModule = angular.module('order.select');
registerModule.directive('submitOrder', ['OrderConfirmationService', 'SelectionModel',
    function (OrderConfirmationService, SelectionModel) {

        var convertQuery = function (selectionModel) {
            var request = "?&id=";
            if(!$.isEmptyObject(selectionModel.basket)){
                var productIdsToPurchase = [];
                $.each(selectionModel.basket, function(index,value) {
                  productIdsToPurchase.push(value.productId);
                });
                request += productIdsToPurchase.join("&id=");
            }
            return request;
        };

        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.bind('click', function (e) {
                    var request = convertQuery(SelectionModel);
                    OrderConfirmationService.confirmOrder(request);
                });
            }
        };
    }]);