// Created by azawisza
angular.module('order.select')
    .controller('SelectController', ['$scope', 'SelectionModel','CatalogueService',
        function ($scope, SelectionModel, CatalogueService) {

            $scope.updateBasket = function (elementForBasket) {
                    console.log("Updating basket: "+elementForBasket);
                    var product = $.parseJSON(elementForBasket.target.className);
                    var productIndexInBasket;
                    var found = $.map(SelectionModel.basket, function(val,index) {
                        if(val.productId == product.productId ){
                            productIndexInBasket = index;
                            return index;
                        }
                    });
                    if(found.length!==0){
                        SelectionModel.basket.splice(productIndexInBasket, 1);
                    }else {
                        SelectionModel.basket.push(product);
                    }
                    SelectionModel.checkoutDisabled = SelectionModel.basket.length<1;
                };

            $scope.$on("customerLocationUpdated", function (event, reload) {
                    console.log("Calling for catalogue for location: "+SelectionModel.customerLocation);
                    CatalogueService.loadCatalogue(SelectionModel.customerLocation);
            });

            $scope.selectionModel = SelectionModel;
        }])
;
