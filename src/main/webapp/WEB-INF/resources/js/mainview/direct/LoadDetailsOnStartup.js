/**
 * Created by cx2 on 2015-08-12.
 */
var registerModule = angular.module('reg.mainview');
registerModule.directive('loadDetailsOnStartup', ['$location', '$rootScope', 'MainViewModel', 'HabitatSelectionModel',
    function ($location, $rootScope, MainViewModel, HabitatSelectionModel) {

        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.ready(function (e) {
                    if (HabitatSelectionModel.selectedHabitat !== null && HabitatSelectionModel.selectedHabitat !== undefined) {
                        $rootScope.$broadcast('habitatsSelectionChanged', 1);
                    }
                });
            }
        };
    }]);
