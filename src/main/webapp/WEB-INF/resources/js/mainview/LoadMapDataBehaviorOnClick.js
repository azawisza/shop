/**
 * Created by cx2 on 2015-08-12.
 */
var registerModule = angular.module('reg.mainview');
registerModule.directive('loadMapDataBehaviorOnClick', ['$location', 'MainViewService', 'MainViewModel',
    function ($location, MainViewService, MainViewModel) {

        var onSuccess = function (value, responseHeaders) {
            MainViewModel.locations = value.data.habitats;
            $rootScope.$broadcast('habitatsUpdated', 1);
        };

        var onFail = function (httpResponse) {
            console.log("failure call result =" + httpResponse);
        };

        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.bind('click', function (e) {
                    MainViewService.loadLocations().then(onSuccess, onFail);
                });
            }
        };
    }]);
