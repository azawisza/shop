/**
 * Created by cx2 on 2015-08-17.
 */
angular.module('reg.mainview').factory('HabitatSelectionModel', [function () {
    var habitatSelectionModel = {
        selectedHabitat: null,
        hideLinkToMap: null,
        habitatDetails: null,
        habitatDetailsOnMapCoordinates: null
    };
    return habitatSelectionModel;
}]);
