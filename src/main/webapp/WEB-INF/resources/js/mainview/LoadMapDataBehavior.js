/**
 * Created by cx2 on 2015-08-12.
 */
var registerModule = angular.module('reg.mainview');
registerModule.directive('loadMapDataBehavior', ['$location', '$rootScope', 'MainViewService', 'MainViewModel',
    function ($location, $rootScope, MainViewService, MainViewModel) {

        var onSuccess = function (value, responseHeaders) {
            MainViewModel.habitats = value.data.habitats;
            $rootScope.$broadcast('habitatsUpdated', 1);
        };

        var onFail = function (httpResponse) {
            console.error("failure call result =" + httpResponse);
        };

        return {
            restrict: 'E',
            link: function (scope, element, attribute) {
                element.ready(function (e) {
                    MainViewService.loadLocations().then(onSuccess, onFail);
                });
            }
        };
    }]);
