var registerModule = angular.module('reg.mainview');
registerModule.factory('MainViewService', ['$resource', '$http',
    function ($resource, $http) {
        return {
            loadLocations: function () {
                return  $http.get('api/all');
            }
        };
    }]);