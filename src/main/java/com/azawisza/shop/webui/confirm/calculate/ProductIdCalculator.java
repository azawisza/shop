package com.azawisza.shop.webui.confirm.calculate;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toSet;

/**
 * Created by azawisza
 */
@Component
public class ProductIdCalculator {

    public Set<Long> toDistinctLong(List<String> productIds) {
        if (productIds == null) {
            return newHashSet();
        }
        Set<String> uniqueProductIds = productIds.stream()
                .filter((e) -> e != null)
                .collect(toSet());
        return uniqueProductIds.stream().map(Long::parseLong)
                .collect(toSet());
    }
}
