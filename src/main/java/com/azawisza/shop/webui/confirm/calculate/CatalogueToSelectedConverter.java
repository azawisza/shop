package com.azawisza.shop.webui.confirm.calculate;

import com.azawisza.shop.services.catalogue.api.Product;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.services.catalogue.api.ProductsCategory;
import com.azawisza.shop.webui.confirm.SelectedProduct;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by azawisza
 */
@Component
public class CatalogueToSelectedConverter {

    public List<SelectedProduct> convertToProducts(Catalogue orders) {
        List<SelectedProduct> result = new ArrayList<>();
        for (ProductsCategory category : orders) {
            Collection<Product> products = category.getProducts();
            result.addAll(products.stream()
                    .map((p) -> new SelectedProduct()
                            .withCategory(p.getCategory())
                            .withName(p.getName()))
                    .collect(toList()));
        }
        return result;
    }
}
