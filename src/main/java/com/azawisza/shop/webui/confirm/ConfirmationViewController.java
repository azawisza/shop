package com.azawisza.shop.webui.confirm;

import com.azawisza.shop.services.audit.EventService;
import com.azawisza.shop.webui.confirm.calculate.ConfirmViewBuilder;
import com.azawisza.shop.webui.confirm.calculate.ViewBuilderCalculator;
import com.azawisza.shop.webui.select.CustomerLoginCalculator;
import com.azawisza.shop.webui.select.CustomerStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * Created by azawisza
 */
@Controller
public class ConfirmationViewController {

    public static final String ORDERS_QUERY_VARIABLE = "id";
    @Autowired
    private ViewBuilderCalculator builderCalculator;
    @Autowired
    private CustomerLoginCalculator calculator;

    @Autowired
    private EventService eventService;

    public
    @RequestMapping(value = "/confirm",
            method = RequestMethod.GET)
    ModelAndView displaySelectPage(@CookieValue(value = "customerID", required = true) String value,
                                   @RequestParam MultiValueMap<String, String> orders, HttpSession session) {
        CustomerStatus customerStatus = calculator.calculateCustomerId(value);
        List<String> products = orders.get(ORDERS_QUERY_VARIABLE);
        if(products!=null){
            eventService.send(products.size());
        }
        session.setAttribute("sessionCustomer",value);
        ConfirmViewBuilder viewBuilder = builderCalculator.calculate(customerStatus, value, products);
        return viewBuilder.build();
    }

}
