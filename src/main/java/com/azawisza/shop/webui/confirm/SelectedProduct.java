package com.azawisza.shop.webui.confirm;

import com.azawisza.shop.common.ToStringSettings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by azawisza
 */
public class SelectedProduct {

    private String category;
    private String name;

    public SelectedProduct withCategory(final String category) {
        this.category = category;
        return this;
    }

    public SelectedProduct withName(final String name) {
        this.name = name;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return ToStringSettings.getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
