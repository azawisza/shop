package com.azawisza.shop.webui.confirm.calculate;

import com.azawisza.shop.webui.confirm.SelectedProduct;
import com.azawisza.shop.webui.select.CustomerStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by azawisza
 */
public class ConfirmViewBuilder {

    public static final String WEBUI_SELECT = "webui/confirm";
    public static final String STATUS_KEY = "status";
    public static final String CUSTOMER_ID_KEY = "customerID";
    public static final String PRODUCTS_KEY = "products";

    private String customerId;
    private List<SelectedProduct> order;
    private CustomerStatus status;

    public static ConfirmViewBuilder confirmationView() {
        return new ConfirmViewBuilder();
    }

    public ModelAndView build() {
        return new ModelAndView(WEBUI_SELECT)
                .addObject(STATUS_KEY, status)
                .addObject(PRODUCTS_KEY, order)
                .addObject(CUSTOMER_ID_KEY, customerId);
    }

    public ConfirmViewBuilder withCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public ConfirmViewBuilder withProducts(List<SelectedProduct> order) {
        this.order = order;
        return this;
    }

    public ConfirmViewBuilder withStatus(CustomerStatus status) {
        this.status = status;
        return this;
    }
}
