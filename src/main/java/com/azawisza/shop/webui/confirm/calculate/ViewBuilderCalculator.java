package com.azawisza.shop.webui.confirm.calculate;

import com.azawisza.shop.services.catalogue.CatalogueServiceImpl;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.webui.select.CustomerStatus;
import com.azawisza.shop.services.catalogue.CatalogueService;
import com.azawisza.shop.webui.confirm.SelectedProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by azawisza
 */

@Component
public class ViewBuilderCalculator {

    private final CatalogueService catalogueService;
    private final CatalogueToSelectedConverter converter;
    private final ProductIdCalculator productIdCalculator;

    @Autowired
    public ViewBuilderCalculator(CatalogueServiceImpl catalogueService,
                                 CatalogueToSelectedConverter converter, ProductIdCalculator productIdCalculator) {
        this.catalogueService = catalogueService;
        this.converter = converter;
        this.productIdCalculator = productIdCalculator;
    }

    public ConfirmViewBuilder calculate(CustomerStatus customerStatus, String id, List<String> products) {
        if (customerStatus != CustomerStatus.USER_ID) {
            return whenInvalid(customerStatus, id);
        } else {
            return calculateForOrders(customerStatus, id, products);
        }
    }

    private ConfirmViewBuilder whenInvalid(CustomerStatus customerStatus, String id) {
        return ConfirmViewBuilder.confirmationView()
                        .withStatus(customerStatus)
                        .withCustomerId(id);
    }

    private ConfirmViewBuilder calculateForOrders(CustomerStatus customerStatus, String id, List<String> products) {
        Set<Long> productIds = productIdCalculator.toDistinctLong(products);
        if (productIds.isEmpty()) {
            return assemblyBuilder(customerStatus, id, Catalogue.empty());

        } else {
            Catalogue catalogue = catalogueService.collectCatalogue(productIds);
            return assemblyBuilder(customerStatus, id, catalogue);
        }
    }

    private ConfirmViewBuilder assemblyBuilder(CustomerStatus status, String id,
                                               Catalogue catalogue) {
        List<SelectedProduct> orders = converter.convertToProducts(catalogue);
        return ConfirmViewBuilder.confirmationView()
                .withStatus(status)
                .withCustomerId(id)
                .withProducts(orders);
    }

}
