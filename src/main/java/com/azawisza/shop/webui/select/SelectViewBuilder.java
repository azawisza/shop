package com.azawisza.shop.webui.select;

import org.springframework.web.servlet.ModelAndView;

/**
 * Created by azawisza
 */
public class SelectViewBuilder {

    public static final String WEBUI_SELECT = "webui/select";
    public static final String STATUS_KEY = "status";
    public static final String CUSTOMER_ID_KEY = "customerID";

    private CustomerStatus status;
    private String customerId;

    private SelectViewBuilder() {
    }

    public SelectViewBuilder ofStatus(CustomerStatus status) {
        this.status = status;
        return this;
    }

    public ModelAndView build() {
        return new ModelAndView(WEBUI_SELECT)
                .addObject(STATUS_KEY, status)
                .addObject(CUSTOMER_ID_KEY, customerId);
    }

    public static SelectViewBuilder selectView() {
        return new SelectViewBuilder();
    }

    public SelectViewBuilder withCustomerId(String custommerId) {
        this.customerId = custommerId;
        return this;
    }
}
