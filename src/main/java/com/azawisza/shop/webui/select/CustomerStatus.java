package com.azawisza.shop.webui.select;

/**
 * Created by azawisza
 */
public enum CustomerStatus {
    NOT_LOGGED_IN,
    INVALID_USER_ID,
    NOT_FOUND,
    USER_ID
}
