package com.azawisza.shop.webui.select;

import org.springframework.stereotype.Component;

/**
 * Created by azawisza
 */
@Component
public class CustomerIdValidator {

    public boolean isValidUserId(String id) {
        try {
            Long.parseLong(id);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;
    }
}
