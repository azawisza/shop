package com.azawisza.shop.webui.select;

import com.azawisza.shop.services.audit.EventService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.azawisza.shop.webui.select.SelectViewBuilder.selectView;

/**
 * Created by azawisza
 */
@Controller
public class SelectionViewController {

    @Autowired
    private CustomerLoginCalculator calculator;

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public ModelAndView displaySelectPage(@CookieValue(value = "customerID", required = false) String value,
                                          HttpSession session) {
        CustomerStatus status = calculator.calculateCustomerId(value);
        Object sessionCustomer = session.getAttribute("sessionCustomer");
        return selectView().ofStatus(status).withCustomerId(value)
                .build();
    }

}
