package com.azawisza.shop.webui.select;

import com.azawisza.shop.services.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by azawisza
 */
@Component
public class CustomerLoginCalculator {

    private final CustomerIdValidator validator;
    private final CustomerRepository repository;

    @Autowired
    public CustomerLoginCalculator(CustomerIdValidator validator, CustomerRepository repository) {
        this.validator = validator;
        this.repository = repository;
    }

    public CustomerStatus calculateCustomerId(String value) {
        CustomerStatus product;
        if (value == null) {
            product = CustomerStatus.NOT_LOGGED_IN;
        } else if (!validator.isValidUserId(value)) {
            product = CustomerStatus.INVALID_USER_ID;
        } else {
            boolean b = repository.customerExists(Long.parseLong(value));
            if (b) {
                product = CustomerStatus.USER_ID;
            } else {
                product = CustomerStatus.NOT_FOUND;
            }
        }
        return product;
    }
}
