package com.azawisza.shop;

import org.springframework.boot.SpringApplication;

import java.util.logging.Logger;

import static java.lang.System.getProperty;
import static java.lang.System.setProperty;

/**
 * Created by azawisza
 */
public class Application {

    private static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    private static final String PROFILE_PROPERTY_NAME = "spring.profiles.active";
    private static final Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WebMvcConfiguration.class);
        String profile = getProperty(PROFILE_PROPERTY_NAME);
        if (profile != null) {
            setProperty(PROFILE_PROPERTY_NAME, profile);
        } else {
            profile = SPRING_PROFILE_DEVELOPMENT;
            setProperty(PROFILE_PROPERTY_NAME, profile);
        }
        logger.info(" Selected profile ----> " + profile);
        application.run(args);
    }
}
