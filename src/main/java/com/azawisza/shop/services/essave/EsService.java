package com.azawisza.shop.services.essave;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.IndicesExists;
import io.searchbox.indices.mapping.PutMapping;
import org.elasticsearch.index.mapper.DocumentMapper;
import org.elasticsearch.index.mapper.core.StringFieldMapper;
import org.elasticsearch.index.mapper.object.RootObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

import org.springframework.stereotype.Component;

/**
 * Created by user on 4/18/17.
 */
@Component
public class EsService {


    private static final int NOT_FOUND_STATUS = 403;
    private static final int FOUND_STATUS = 200;


    private JestClient jestClient;

    @Autowired
    public EsService(JestClient jestClient){
        this.jestClient = jestClient;
    }

    public void createIndexIfNotExists() {
        String indexName  ="metrics-index2";
        try {
            JestResult result = jestClient.execute(new IndicesExists.Builder(indexName).build());
            System.out.println("Checking if index already exist."+result);
            int responseCode = result.getResponseCode();
            if (responseCode == FOUND_STATUS) {
                //CreateIndex createIndex = new CreateIndex.Builder(indexName).build();
                RootObjectMapper.Builder rootObjectMapperBuilder = new RootObjectMapper.Builder("my_mapping_name").add(
                        new StringFieldMapper.Builder("message").store(true)
                );
                DocumentMapper documentMapper = new DocumentMapper.Builder(rootObjectMapperBuilder, null).build(null);
                String expectedMappingSource = documentMapper.mappingSource().toString();
                PutMapping putMapping = new PutMapping.Builder(
                        "my_index",
                        "my_type",
                        expectedMappingSource
                ).build();
                JestResult execute = jestClient.execute(putMapping);
                System.out.println("err="+execute.getErrorMessage());
            }
        } catch (IOException e) {
            System.out.println("Something went wrong during creation of new index"+e);
            e.printStackTrace();
        }
    }

}
