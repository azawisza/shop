package com.azawisza.shop.services.catalogue;

import com.azawisza.shop.services.catalogue.api.Product;
import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.services.catalogue.domain.CatalogueRepository;
import com.azawisza.shop.services.catalogue.domain.ProductEntity;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by azawisza
 */
@Component
public class CatalogueServiceImpl implements CatalogueService {


    private final CatalogueRepository repository;

    @Autowired
    public CatalogueServiceImpl(CatalogueRepository repository) {
        this.repository = repository;
    }

    public Catalogue getCatalogueForLocation(String location) {
        List<ProductEntity> allProducts = repository.getAllProducts(location);
        List<Product> converted = convertToService(allProducts);
        Multimap<String, Product> byCategory = groupedByCategory(converted);
        return new Catalogue(byCategory);
    }

    public Catalogue collectCatalogue(Set<Long> productIds) {
        List<ProductEntity> products = repository.findProducts(productIds);
        List<Product> converted = convertToService(products);
        Multimap<String, Product> byCategory = groupedByCategory(converted);
        return new Catalogue(byCategory);
    }

    private Multimap<String, Product> groupedByCategory(List<Product> converted) {
        Multimap<String, Product> result = ArrayListMultimap.create();
        for (Product product : converted) {
            result.put(product.getCategory(), product);
        }
        return result;
    }

    private List<Product> convertToService(List<ProductEntity> allProducts) {
        return allProducts.stream().map((e) -> new Product().withLocation(e.getLocation())
                .withCategory(e.getCategory())
                .withName(e.getName())
                .withProductId(Long.toString(e.getProductId()))).collect(Collectors.toList());
    }

}
