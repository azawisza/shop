package com.azawisza.shop.services.catalogue.domain;

import javax.persistence.*;

/**
 * Created by azawisza
 */
@Entity
@Table(name = "product")
public class ProductEntity {

    @Id
    @Column
    @GeneratedValue
    private long productId;

    @Column
    private String category;

    @Column
    private String location;

    @Column(unique = true, nullable = false)
    private String name;

    public ProductEntity withProductId(final long productId) {
        this.productId = productId;
        return this;
    }

    public ProductEntity withCategory(final String category) {
        this.category = category;
        return this;
    }

    public ProductEntity withLocation(final String location) {
        this.location = location;
        return this;
    }

    public ProductEntity withName(final String name) {
        this.name = name;
        return this;
    }

    public long getProductId() {
        return productId;
    }

    public String getCategory() {
        return category;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }
}
