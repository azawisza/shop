package com.azawisza.shop.services.catalogue.api;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import static com.azawisza.shop.common.ToStringSettings.getToStringBuilder;

/**
 * Created by azawisza
 */
public class Product {

    private String productId;
    private String category;
    private String location;
    private String name;

    public String getProductId() {
        return productId;
    }

    public String getCategory() {
        return category;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public Product withProductId(final String productId) {
        this.productId = productId;
        return this;
    }

    public Product withCategory(final String category) {
        this.category = category;
        return this;
    }

    public Product withLocation(final String location) {
        this.location = location;
        return this;
    }

    public Product withName(final String name) {
        this.name = name;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
