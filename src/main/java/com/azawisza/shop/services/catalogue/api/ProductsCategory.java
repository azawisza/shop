package com.azawisza.shop.services.catalogue.api;

import com.azawisza.shop.common.ToStringSettings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Collection;
import java.util.List;

/**
 * Created by azawisza
 */
public class ProductsCategory {

    private String name;
    private Collection<Product> products;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public ProductsCategory withName(final String name) {
        this.name = name;
        return this;
    }

    public ProductsCategory withProducts(final Collection<Product> products) {
        this.products = products;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return ToStringSettings.getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
