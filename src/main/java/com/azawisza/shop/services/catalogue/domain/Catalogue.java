package com.azawisza.shop.services.catalogue.domain;

import com.azawisza.shop.services.catalogue.api.Product;
import com.azawisza.shop.services.catalogue.api.ProductsCategory;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by azawisza
 */
public class Catalogue implements Iterable<ProductsCategory> {

    private final Multimap<String, Product> byCategory;

    public Catalogue(Multimap<String, Product> byCategory) {
        this.byCategory = byCategory;
    }

    public static Catalogue empty() {
        return new Catalogue(ArrayListMultimap.create());
    }

    @Override
    public Iterator<ProductsCategory> iterator() {
        return new ProductsCategoryIterator(byCategory.asMap().entrySet());
    }

    private static class ProductsCategoryIterator implements Iterator<ProductsCategory> {

        private final Iterator<Map.Entry<String, Collection<Product>>> entries;

        public ProductsCategoryIterator(Set<Map.Entry<String, Collection<Product>>> entries) {
            this.entries = entries.iterator();
        }

        @Override
        public boolean hasNext() {
            return entries.hasNext();
        }

        @Override
        public ProductsCategory next() {
            Map.Entry<String, Collection<Product>> next = entries.next();
            return new ProductsCategory()
                    .withName(next.getKey())
                    .withProducts(next.getValue());
        }
    }
}
