package com.azawisza.shop.services.catalogue.domain;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Set;

/**
 * Created by azawisza
 */
@Repository
public class CatalogueRepository {

    @PersistenceContext
    public EntityManager entityManager;


    @Transactional
    public List<ProductEntity> getAllProducts(String location) {
        TypedQuery<ProductEntity> query = entityManager.createQuery("select p from ProductEntity p where p.location=:locationp or p.location is null", ProductEntity.class);
        query.setParameter("locationp", location);
        return query.getResultList();
    }

    @Transactional
    public List<ProductEntity> findProducts(Set<Long> ids) {
        List<ProductEntity> result = Lists.newArrayList();
        for (Long id : ids) {
            ProductEntity productEntity = entityManager.find(ProductEntity.class, id);
            if (productEntity != null) {
                result.add(productEntity);
            }
        }
        return result;
    }

}
