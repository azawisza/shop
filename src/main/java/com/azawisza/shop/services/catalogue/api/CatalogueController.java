package com.azawisza.shop.services.catalogue.api;

import com.azawisza.shop.services.catalogue.domain.Catalogue;
import com.azawisza.shop.services.catalogue.CatalogueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by azawisza
 */
@RestController
public class CatalogueController {

    private final CatalogueService service;

    @Autowired
    public CatalogueController(CatalogueService service) {
        this.service = service;
    }

    public
    @RequestMapping(value = "/catalogue/{location}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    CatalogueRS getLocationCatalogue(@PathVariable(required = true) String location, @CookieValue(value = "customerID", required = true) String value) {
        Catalogue catalogue = service.getCatalogueForLocation(location);
        return buildResponse(catalogue);
    }

    private CatalogueRS buildResponse(Catalogue catalogue) {
        CatalogueRS catalogueRS = new CatalogueRS();
        for (ProductsCategory category : catalogue) {
            catalogueRS.withCategory(category);
        }
        return catalogueRS;
    }

}
