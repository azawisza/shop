package com.azawisza.shop.services.catalogue;

import com.azawisza.shop.services.catalogue.domain.Catalogue;

import java.util.Set;

/**
 * Created by azawisza
 */
public interface CatalogueService {

    public Catalogue getCatalogueForLocation(String location);

    public Catalogue collectCatalogue(Set<Long> products);

}
