package com.azawisza.shop.services.catalogue.api;

import com.azawisza.shop.common.ToStringSettings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azawisza
 */
public class CatalogueRS {

    private List<ProductsCategory> categories;

    public CatalogueRS withCategory(ProductsCategory allProducts) {
        if (this.categories == null) {
            this.categories = new ArrayList<>();
        }
        this.categories.add(allProducts);
        return this;
    }

    public List<ProductsCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<ProductsCategory> categories) {
        this.categories = categories;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return ToStringSettings.getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
