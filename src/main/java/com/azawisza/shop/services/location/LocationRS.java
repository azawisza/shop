package com.azawisza.shop.services.location;

import com.azawisza.shop.common.ToStringSettings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by azawisza
 */
public class LocationRS {

    private String locationId;

    public LocationRS withLocation(final String location) {
        this.locationId = location;
        return this;
    }

    public String getLocationId() {
        return locationId;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return ToStringSettings.getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


}
