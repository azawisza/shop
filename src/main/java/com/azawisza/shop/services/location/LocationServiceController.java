package com.azawisza.shop.services.location;

import com.azawisza.shop.services.audit.EventService;
import com.azawisza.shop.webui.select.CustomerIdValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by azawisza
 */
@RestController
public class LocationServiceController {

    @Autowired
    private CustomerLocationService locationService;

    @Autowired
    private EventService eventService;

    @Autowired
    public CustomerIdValidator validator;

    public
    @RequestMapping(value = {"/customerlocation/", "/customerlocation"},
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    LocationRS getCustomerLocation(@CookieValue(value = "customerID", required = true) String value) {
        LocationRS response = new LocationRS();
        eventService.send("location:"+value);
        boolean validUserId = validator.isValidUserId(value);
        if(!validUserId ){
            return response;
        }
        Optional<String> locationId = locationService.findCustomerLocation(value);
        if (locationId.isPresent()) {
            response.withLocation(locationId.get());
        }
        return response;
    }

}
