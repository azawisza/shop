package com.azawisza.shop.services.location;

import java.util.Optional;

/**
 * Created by azawisza
 */
public interface CustomerLocationService {

    public Optional<String> findCustomerLocation(String customerId);

}
