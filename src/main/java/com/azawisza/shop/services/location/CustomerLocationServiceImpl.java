package com.azawisza.shop.services.location;

import com.azawisza.shop.services.customer.CustomerEntity;
import com.azawisza.shop.services.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

/**
 * Created by azawisza
 */
@Component
public class CustomerLocationServiceImpl implements CustomerLocationService {

    private CustomerRepository repository;

    @Autowired
    public CustomerLocationServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    /*Service stub retrieving information saved in db for consistency
        * */
    public Optional<String> findCustomerLocation(String customerId) {
        Long aLong = Long.valueOf(customerId);
        Optional<CustomerEntity> customer = repository.findCustomer(aLong);
        if (customer.isPresent()) {
            CustomerEntity customerEntity = customer.get();
            String location = customerEntity.getLocation();
            return ofNullable(location);
        } else {
            return empty();
        }
    }
}
