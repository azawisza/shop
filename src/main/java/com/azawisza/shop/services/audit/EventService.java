package com.azawisza.shop.services.audit;

import com.azawisza.shop.services.essave.EsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by user on 4/17/17.
 */
@Component
public class EventService {

    @Autowired
    public MessageService messageService;

    @Autowired
    public EsService esService;

    public void send(String messge){
        messageService.sendMessage(messge);
        esService.createIndexIfNotExists();
    }

    public void send(int messge){
        messageService.sendMessage(String.valueOf(messge));
        esService.createIndexIfNotExists();
    }

}
