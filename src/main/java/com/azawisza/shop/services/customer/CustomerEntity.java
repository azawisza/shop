package com.azawisza.shop.services.customer;

import javax.persistence.*;

/**
 * Created by azawisza
 */
@Entity
@Table(name = "customer")
public class CustomerEntity {

    @Version
    private long version;

    @Id
    @Column
    private long customerId;

    @Column
    private String location;


    public CustomerEntity withCustomerId(final long customerId) {
        this.customerId = customerId;
        return this;
    }

    public CustomerEntity withLocation(final String location) {
        this.location = location;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public String getLocation() {
        return location;
    }


}
