package com.azawisza.shop.services.customer;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import java.util.Optional;

/**
 * Created by azawisza
 */
@Component
public class CustomerRepository {

    @PersistenceContext
    public EntityManager entityManager;

    @Transactional
    public Optional<CustomerEntity> findCustomer(long customerId) {
        CustomerEntity customerEntity = entityManager.find(CustomerEntity.class, customerId, LockModeType.OPTIMISTIC);
        return Optional.ofNullable(customerEntity);
    }

    @Transactional
    public boolean customerExists(Long customerId) {
        Optional<CustomerEntity> customer = findCustomer(customerId);
        return customer.isPresent();
    }
}
