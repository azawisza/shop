package com.azawisza.shop.configuration;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * Created by azawisza
 */
public class SessionInitializer extends AbstractHttpSessionApplicationInitializer {

    public SessionInitializer() {
        super(RedisSession.class);
    }

}
