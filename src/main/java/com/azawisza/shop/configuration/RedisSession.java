package com.azawisza.shop.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * Created by azawisza
 */
@EnableRedisHttpSession
public class RedisSession {

    @Value("${spring.redis.host}")
    public String host;

    @Value("${spring.redis.port}")
    public int port;

    @Bean
    public LettuceConnectionFactory connectionFactory() {

        return new LettuceConnectionFactory(host, port);
    }
    //Caused by: com.lambdaworks.redis.RedisCommandExecutionException: ERR unknown command 'CONFIG'
    //disabling config action Caused by: com.lambdaworks.redis.RedisCommandExecutionException: ERR unknown command 'CONFIG'
    @Bean
    public static ConfigureRedisAction configureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }


}
