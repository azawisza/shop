package com.azawisza.shop.configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.azawisza.shop.services.audit.SQSListener;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vc.inreach.aws.request.AWSSigner;
import vc.inreach.aws.request.AWSSigningRequestInterceptor;

import java.time.LocalDateTime;

import org.springframework.context.annotation.Configuration;

/**
 * Created by user on 4/18/17.
 */
@Configuration
public class ElasticSearchConfig {

    @Autowired
    private SQSListener sqsListener;
    @Value("${queue.accessKey}")
    private String accessKey;
    @Value("${queue.secretKey}")
    private String secretKey;

    @Value("${queue.endpoint}")
    private String esEndpoint;


    private static final String ES_REGION = "us-east-1";
    private static final String ES_SERVICE_PREFIX = "es";

    @Bean
    public JestClient jestClient() {

        final JestClientFactory factory = getJestClientFactory();

        factory.setHttpClientConfig(new HttpClientConfig
                .Builder(esEndpoint)
                .multiThreaded(true)
                .build());

        return factory.getObject();
    }

    private JestClientFactory getJestClientFactory() {

        return new JestClientFactory() {

            @Override
            protected HttpClientBuilder configureHttpClient(HttpClientBuilder builder) {
                builder.addInterceptorLast(prepareInterceptor());
                return builder;
            }

            @Override
            protected HttpAsyncClientBuilder configureHttpClient(HttpAsyncClientBuilder builder) {
                builder.addInterceptorLast(prepareInterceptor());
                return builder;
            }
        };
    }

    private AWSSigningRequestInterceptor prepareInterceptor() {
        AWSSigner signer = new AWSSigner(createCredentialsProvider(), ES_REGION, ES_SERVICE_PREFIX, LocalDateTime::now);
        return new AWSSigningRequestInterceptor(signer);
    }

    private AWSCredentialsProvider createCredentialsProvider() {
        AWSCredentialsProvider awsCredentialsProvider = new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return new BasicAWSCredentials(accessKey, secretKey);
            }
            @Override
            public void refresh() {
            }
        };
        return awsCredentialsProvider;
    }
}