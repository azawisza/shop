In order to build and run locally:, please enter the director with pom and execute:
mvn clean install
cd target
java -jar shop.jar

The application is available under http://localhost:8070/select

The database is available under:
http://localhost:8082/
Driver:    org.h2.jdbcx.JdbcDataSource
JdbcUrl: jdbc:h2:mem:shop
User: sa
Password:  there is no pasword, leave the field empty

Data are loaded automatically after start from import.sql file

In order to use application set cookie with value customerID=1 or  customerID=2 using postman chrome plugin.